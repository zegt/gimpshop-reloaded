GIMPshop Reloaded
======
###Introduction
GIMPshop was a modification of the free and open source graphics program GNU Image Manipulation Program (GIMP), with the intent to replicate the feel of Adobe Photoshop. 
Its primary purpose was to make users of Photoshop feel comfortable using GIMP (en.wikipedia.org)

GIMPshop Reloaded is an original GIMPshop remake, made by someone who had been using Photoshop for seven years! 
GIMPshop Reloaded has an interface similiar to Adobe's product, shortcuts, plugin and much more, everything you need to have a real Photoshop feel.

### How to install
##### Linux

`
git clone git://github.com/zegt/gimpshop-reloaded ~/.gimpshop-temp
`

`
rm ~/.gimpshop-temp/gimprc
`

Copy `~/.gimpshop-temp` content into `~/.gimp-2.8`

`
rmdir ~/.gimpshop-temp
`

##### Windows
_coming soon..._

##### Mac OS X
_coming soon..._

### Thanks to
- [**epierce**](http://epierce.freeshell.org/gimp/gimp_ps.php) for Photoshop shortcut.
- [**Mehdi Abdollahi**](#) for GIMP Theme.
- [**Riley Brandt**](http://www.rileybrandt.com/) for some interface fixes.
- [**slybug**](http://slybug.deviantart.com) for layer via copy/cut plugin.

### License
GIMPshop Reloaded is published under GNU General Public License v3.0.
